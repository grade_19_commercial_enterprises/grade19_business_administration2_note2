#  前台程序
```
   占用终端（session）的程序，叫前台程序
```

# 后台程序
```
不占用终端（session）标准输入的程序，叫后台程序
```

# 如何将一个前台程序运行在后台
```
node main.js

node main.js &
```

# 能更好的将一个前台程序运行在后台的方法
+ disown
+ nohup
+ pm2、forever pm2的负载平衡
+ systemd

## disown 命令
```
不挂断运行中的命令
```

+ 移出所有正在执行的后台任务
   disown -r

+ 移出所有后台任务
   disown -a

+ 不移出后台任务，但是让它们不会收到SIGHUP信号
   disown -h

+ 根据jobId，移出指定的后台任务
   disown %2
   disown -h %2

## nohup 命令
```
jobs -l

ps -ef

a:显示所有程序 
 u:以用户为主的格式来显示 
 x:显示所有程序，不以终端机来区分
 
另外也可以使用 ps -def | grep "runoob.sh" 命令来查找。
找到 PID 后，就可以使用 kill PID 来删除。
kill -9  进程号PID
注：

　　用ps -def | grep查找进程很方便，最后一行总是会grep自己

　　用grep -v参数可以将grep命令排除掉
```

