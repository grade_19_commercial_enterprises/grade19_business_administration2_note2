# 课堂笔记
## 记录日志文件的服务
    1、系统常用的日志（日志是用来记录重大事件的工具） 

        /var/log/message      系统信息日志，包含错误信息等

        /var/log/secure         系统登录日志

        /var/log/cron            定时任务日志

        /var/log/maillog         邮件日志

        /var/log/boot.log       系统启动日志

    2、日志管理服务 rsyslog

        【1】作用：主要用来采集日志，不产生日志

        【2】配置文件：/etc/rsyslog.conf

    编辑文件时的格式为：  ------  *.*           存放日志文件 ------

            其中第一个*代表日志类型，第二个*代表日志级别


### 1.日志类型分为：

        auth            　　##pam产生的日志

        authpriv             ##ssh、ftp等登录信息的验证信息

        corn             　  ##时间任务相关

        kern           　　 ##内核

        lpr              　　 ##打印

        mail                    ##邮件

        mark(syslog)-rsyslog##服务内部的信息，时间标识

        news　　##新闻组

        user　　##用户程序产生的相关信息

        uucp　　##unix  to  nuix  copy主机之间相关的通信

        local  1-7　　##自定义的日志设备


### 2. 日志级别分为：

        debug　　##有调试信息的，日志通信最多

        info　　　 ##一般信息日志，最常用

    　 notice　　 ##最具有重要性的普通条件的信息

    　 warning　  ##警告级别

    　 err　　　  ##错误级别，阻止某个功能或者模块不能正常工作的信息

        crit　　　  ##严重级别，阻止整个系统或者整个软件不能正常工作的信息

        alert　　   ##需要立刻修改的信息

        emerg　　 ##内核崩溃等重要信息

        none　　   ##什么都不记录 

        注意：从上到下，级别从低到高，记录信息越来越少

        
### 3.日志的远程同步 

        【1】 作用：便于管理多台主机

        【2】步骤： 

    　　　　（1）在日志发送方：vim  /etc/rsyslog.conf  ----->文件里添加内容： *.*@172.25.254.97	    ------>  systemctl  restart  rsyslog

    　　　　　　注释：添加内容里面，@表示使用udp协议发送；@@表示使用tcp协议发送 

    　　　　（2）在日志接收方：vim  /etc/rsyslog.conf ------>使第15行、16行有效------> systemctl restart rsyslog ------>systemctl stop firewalld

    　　　　　　　　　　　　　　------>systemctl   disable firewalld

        　　　　 注释： 配置文件里面，15行：$ModLoad  imudp ##表示日志接受模块;

        　　　　　　　　　　　　　　　 16行：$UDPServerRun 514##表示开启接受模块

        　　　　　　　　　　　　　　　systemctl   stopfirawalld##关闭接受方防火墙 

        　　　　　　　　　　　　　　　  systemctl    disable  firewalld   ##使防火墙开机不自启动 

    　　　　（3）发送方和接收方均清空日志文件，使用命令： > /var/log/message 

    　　　　（4）测试：在日志的发送方运行：logger   test  ------> cat /var/log/message   ;完成之后，会产生日志信息

    　　　　　　　　　  在接受方运行：cat    /var/log/message ; 

## 日志文件的安全设置
    1.取消或添加文件的隐藏权限
    使用+ -加权限类型即可设置文件的隐藏权限

    chattr -i //减去文件的 i 隐藏数字属性,然后即可使用rm 正常删除
    chattr +i //添加隐藏权限
    =：将隐藏权限设置为该值；

　　chattr：权限
　　A：文件在存取过程中不会修改atime；
　　S：一般文件并不是同步写入到硬盘中，添加这个属性后，则会同步；
　　a：添加后，改文件只能增加，不能修改也不能删除，需要使用root账号；
　　c：文件会自动压缩，读取时会自动解压缩；
　　d：不能使用dump程序进行备份；
　　i：不能修改，添加，删除，修改名字等一切操作，需要root账号；
　　s：删除时直接从硬盘中移除不能恢复；
　　u：删除后仍然会保存在硬盘中，可以恢复；



# 懵了懵了懵了懵了懵了懵了懵了！！！！！！！