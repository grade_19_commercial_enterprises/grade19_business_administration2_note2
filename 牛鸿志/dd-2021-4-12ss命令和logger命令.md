1.隐藏权限 
chattr +a 日志文件   //给日志文件加上隐藏权限，以使文件只能添加，不能删除（root也不行）
chattr -a 取消隐藏权限


2.日志文件服务器
支持UDP和TCP协议  区别  UDP路径传输很多　TCP路径传输一致


3.查看日志  tail  -f 日志
4.systemctl restart rsyslog.service 重启日志服务
5.systenctl status rsyslog 查看
6.在/var/log路径下 输入 cat messages 查找添加的日志
7.vim /etc/rsyslog.d/jj.conf   日志配置文件
8.ss 命令详解   ss -lnp  | grep -i 514 查找 514日志
-t： tcp

  -a:  all

  -l:  listening         【ss -l列出所有打开的网络连接端口】

  -s:  summary        【显示 Sockets 摘要】

  -p:  progress

  -n:  numeric         【不解析服务名称】

  -r:  resolve        【解析服务名称】

  -m: memory        【显示内存情况】
9.logger 命令  添加日志  logger "..."
添加前 要配置日志文件 vim /etc/rsyslog.d/ds.conf
//内容  *.*  @@172.16.10.213
