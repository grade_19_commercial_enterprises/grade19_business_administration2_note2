## 设置一个静态网页

```
1.cd ..  #退到/目录
2.su     #进入超级管理员模式
3.mkdir /var/www  #创建目录
4.cd /var/www   #进入
5.mkdir /var/www/ooo.com(域名)    #在目录底下创建文件连接网页 并编辑
6.cd ooo.com      #进入目录
7.vim index.html      #在目录底下创建文件连接网页     
###
<html>
      <head>
      <meta charset="utf8"/>
      </head>
      <body>
      <h1>内容</h1>
      
      
      </body>
      
      



</html>
8.vim /etc/nginx/conf.d/ooo.com.conf    #配置文件
###
server {
        listen 80;  #监听端口
        server_name ooo.com ;  #监听的域名
        location / {

                root /var/www/ooo.com ;#网站的所有路径、
                index index.html ;

        }  



}
9.cd /usr/sbin      #进入路径
10. ./nginx -t     #查看状态
11. ./nginx -s reload    #刷新
12.调式  
13.网页搜索

```

