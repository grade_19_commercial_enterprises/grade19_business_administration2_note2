## 在Linux中布署ftp服务器

### 先下载FileZilla服务

### 在Linux中安装vsftpd软件
+ yum install -y vsftpd
![图片](./img/2021-07-22-01.png)

### 创建用户
![图片](./img/2021-07-22-02.png)

### 在user_list文件中添加创建的用户
+ vim user_list
![图片](./img/2021-07-22-03.png)

### 配置vsftpd.conf文件
+ vim vsftpd.conf
![图片](./img/2021-07-22-04.png)

![图片](./img/2021-07-22-05.png)

![图片](./img/2021-07-22-06.png)

### 重启服务
+ systemctl restart vsftpd.service
+ ftp ip地址(ftp连接服务器的IP)
![图片](./img/2021-07-22-07.png)

### 修改/home底下刚创建用户文件夹的权限
+ chmod 777 testftp
![图片](./img/2021-07-22-08.png)

### 登录及访问
![图片](./img/2021-07-22-09.png)

![图片](./img/2021-07-22-10.png)


