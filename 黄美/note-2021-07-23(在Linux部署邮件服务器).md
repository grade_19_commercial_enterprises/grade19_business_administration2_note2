## 在Linux部署邮件服务器

### 一、安装postfix服务
+ yum -y install postfix

### 二、配置postfix文件
+ cd /etc/postfix  //进入postfix
+ cp main.cf main.cf.bak //备份
+ vim main.cf
![图片](./img/2021-07-23-01.png)

![图片](./img/2021-07-23-02.png)

![图片](./img/2021-07-23-03.png)

![图片](./img/2021-07-23-04.png)

+ postfix check 检查postfix服务
![图片](./img/2021-07-23-05.png)

### 三、重启postfix服务
+ systemctl restart postfix   重启postfix状态
+ systemctl enable postfix     设置开机自启动
+ systemctl status postfix     查看postfix服务状态

### 四、安装dovecot
+ yum -y install dovecot

### 五、配置dovecto文件
+ cd /etc/dovecot 切换到dovecot
+ vim dovecot.conf 进入主配置文件进行修改
![图片](./img/2021-07-23-06.png)

+ cd /etc/dovecpt/conf.d 进入子配置文件进行修改
+ vim 10-auth.conf
![图片](./img/2021-07-23-07.png)

+ vim 10-ssl.conf
![图片](./img/2021-07-23-08.png)

+ vim 10-mail.conf
![图片](./img/2021-07-23-09.png)

### 六、重启dovecot服务
+ systemctl restart dovecot   重启dovecot状态
+ systemctl enable dovecot     设置开机自启动
+ systemctl status dovecot     查看dovecot服务状态
![图片](./img/2021-07-23-10.png)

### 七、创建用户，设置密码
+ useradd usermail
+ echo "mail123456" | passwd --stdin usermail
+ useradd user
+ echo "mail123456" | passwd --stdin user
![图片](./img/2021-07-23-11.png)

### 八、创建INBOX文件夹
```
若不设置，后面接受邮件时会报错
mkdir -p /home/usermail/mail/.imp/INBOX
mkdir -p /home/user/mail/.imp/INBOX
chown -R usermail.usermail /home/usermail
chown -R user.user /home/user
```
![图片](./img/2021-07-23-12.png)

### 九、端口的设置
1. 在阿里云中开启143端口、25端口、110端口
2. 解析域名
![图片](./img/2021-07-23-13.png)
