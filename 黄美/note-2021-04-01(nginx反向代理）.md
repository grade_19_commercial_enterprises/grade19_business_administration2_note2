## 2021年4月1日nginx反向代理的课堂笔记

### 什么是反向代理
```
反向代理是指以代理服务器来接受Internet上的连接请求，然后将请求转发给内部或其他网络上的服务器，并将从服务器上得到的结果返回给Internet上请求连接的客户端。
```

### 配置反向代理
```
// 设置配置文件
# vim etc/nginx/conf.d/xxx.com
//文件添加内容
upstream test{
     #添加对远程服务器的代理，即对windows开发环境进行代理
    server 172.16.10.214;
}
server{
    listen 80;//nginx监听端口
    server_name xxx.com;//需要代理的域名名称
    #index index.html;
    location / {
        proxy_pass http://172.16.10.214：3001；#代理的ip地址和端口号
        
        proxy_redirect off;
        
        proxy_set_header X-Real-IP $remote_addr;
        
        roxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        
        #代理的连接超时时间（单位：毫秒）
        proxy_connect_timeout 600;
        
        #代理的读取资源超时时间（单位：毫秒）
        proxy_read_timeout 600;

    }
}
#cd ../sbin

//检查配置文件xx.com
#./nginx -t

//重新载入配置文件
#./nginx -s reload
```



