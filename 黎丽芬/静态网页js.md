# 一、步骤
## 1，建文件
    # mkdir -p /var/www/yoshi.com
    # cd /var/www/yoshi.com
    # vim main.js
        'use strict';
        let http=require('http');
        let server=http.createServer((req,res)=>{
            res.writeHead(200);

            res.write('<head><meta charset="utf8"></meta></head>');
            res.end('You only win when you maintain your dignity.')
        });

        let port=7500;

        server.listen(port);

        console.log('当前服务运行在如下地址：http://127.0.0.1:${port}');

## 2,运行
    # node main.js &

## 3,浏览器查看
    172.16.10.213：7500

# 二、补充

## 1，前台程序和后台程序的区别
    后台程序不接受标准输入

## 2，后台程序特性
    a、后台程序接受标准输出和错误输出
    b、后台程序不接受标准输入
    c、后台程序（对大多数Linux系统来说），不响应（不接受）SIGNUP信号，所以后台程序此时不会随着终端的退出而退出

## 3，实现类似服务的命令
    a、利用&符号，将程序运行在后台
	b、利用disown命令，将程序运行在后台 借助这个命令运行的进程，不接受SIGNUP这个信号，
	所以，不会因为终端退出而结束程序的运行，但是，如果程序中，尝试输出内容到终端，则终端退出，进程跟着退出
	c、利用nohup命令，将程序运行在后台 将运行中的程序（进程）的输出自动的重定向到一个不存在的设备，
	这样一来，终端退出，进程并不会跟着退出
	d、利用外部程序pm2，将程序运行在后台
	e、利用systemd系统本身的属性，将程序作为服务运行在后台




