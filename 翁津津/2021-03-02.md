# 2021-03-02
# 计划任务
## 1.计划任务的种类
```
at:突发性，处理仅执行一次就结束的命令 配套使用atd服务
cron:例行性，使所设置的任务循环执行下去 配套使用crond服务
```
### at、cron是个程序，atd、crond是个服务
## 2.安装/删除at
```
yum -y install at  安装
yum -y remove at   删除
```
![图片](./images/2021-03-03_1.png)
![图片](./images/2021-03-03_5.png)
## 3.查看atd目前状态
```
systemctl status atd
当看到[enabled][running]时，才能证明atd是真的在运行
```
![图片](./images/2021-03-03_2.png)
## 4.atd的启动
```
systemctl restart atd  重启atd
systemctl enable atd   开机启动atd
systemctl disable      开机不启动atd
```
![图片](./images/2021-03-03_3.png)
```
红色是开机不启动的情况，所以那是他的默认位置
黄色是开机启动的情况，symlink是个符号链接，可以链接到graphical.target(图形界面)或者multi-user.target(多用户命令行)
```
## 5.atd的启用/结束
```
systemctl start atd  启用
systemctl stop atd   结束
```
![图片](./images/2021-03-03_4.png)
