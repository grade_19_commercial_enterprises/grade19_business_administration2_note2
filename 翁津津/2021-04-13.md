# 2021-04-13
## 1.隐藏权限
```
chattr +a 日志文件
给日志文件加上特殊权限，使日志内容只能新增内容，不能删除(包括root)
chattr -a 日志文件
解除特殊权限
```
## 2.日志文件服务器的设置/操作
```
服务端：
1.vim /etc/resyslog.conf
2.修改端口号 514 即去掉#
3.若是在自己的服务器上，还要去阿里云里打开514端口+
3.systemctl restart rsyslog
4.systemctl status rsyslog
```
![图片](./images/2021-04-13_3.png)
![图片](./images/2021-04-13_4.png)
```
客户端：
1.vim /etc/rsyslog.d/xx.conf
2.在里面输入：*.*   @@端口号
3.systemctl restart rsyslog
4.systemctl status rsyslog
```
![图片](./images/2021-04-13_6.png)
![图片](./images/2021-04-13_7.png)
```
查询：
1.在客户端输入 logger "xxx"
2.在服务端cd /var/log
3.服务端 tail -f messages
```
![图片](./images/2021-04-13_8.png)
![图片](./images/2021-04-13_9.png)
```
查看514端口是否开启：ss -lnp |grep -i 514
```
![图片](./images/2021-04-13_5.png)
## 3.日志文件的轮循
```
logrotate这个程序的参数配置文件在/etc/logrotate.conf
```
![图片](./images/2021-04-13_2.png)
```
weekly:默认每个星期对日志文件进行一次轮循
rotate 4:保留几个日志文件，默认4个
create：由于日志文件被更名，所以得建立个新的
dateext:可让被轮循的文件名称加上日期
compress:被修改日志文件是否需要压缩，日志文件太大可考虑使用
```
![图片](./images/2021-04-13_1.png)