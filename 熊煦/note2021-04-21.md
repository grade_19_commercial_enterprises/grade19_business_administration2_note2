## CentOS7安装防火墙
```
安装并启用FirewallD

01、安装FirewallD

默认情况下，Firewalld安装在CentOS 7上，但如果系统上未安装，则可以通过键入以下命令安装软件包：

sudo yum install firewalld

02、检查防火墙状态。

默认情况下禁用Firewalld服务。 您可以使用以下命令检查防火墙状态

sudo firewall-cmd --state

如果您刚刚安装或从未激活过，则该命令将打印不运行，否则您将看到正在运行。

03、启用FirewallD

要启动FirewallD服务并在引导类型上启用它：

sudo systemctl start firewalld
sudo systemctl enable firewalld
```