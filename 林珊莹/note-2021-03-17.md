# 一、静态网站的部署

## 以下一切程序由root用户

## 1、检查是否安装nginx

```
未安装nginx时CentOS8是用yum -y install nginx
Debian是用apt -y install nginx
```

```
安装完之后用systemctl status nginx看nginx状态
```

### 若nginx启动
![图片](./img/six/1.png)

### 若nginx未启动
![图片](./img/six/2.png)

### 未启动的话要将nginx启动
![图片](./img/six/3.png)

## 2、建立所有的网站的总目录，在网站总目录下创建一个网站（zzlin.icu是我自己的域名），命令如下：

![图片](./img/six/4.png)

### 进入自己的目录之后
![图片](./img/six/5.png)

## 3、在/etc/nginx/conf.d这个目录下创建一个zzlin.icu.conf的配置

![图片](./img/six/6.png)
![图片](./img/six/7.png)

## 4、nginx -t测试文件是否有问题

![图片](./img/six/8.png)

## 5、如果配置文件时成功的，则使用nginx -s reload重新加载配置文件

![图片](./img/six/9.png)


## 6、在本地网上部署静态网站

### 配置文件成功时，在debian下要去修改host文件

```
host文件路劲在C盘/system32/drivers/etc/host
```

![图片](./img/six/10.png)
![图片](./img/six/11.png)
![图片](./img/six/12.png)
![图片](./img/six/13.png)
![图片](./img/six/14.png)
![图片](./img/six/15.png)
![图片](./img/six/16.png)