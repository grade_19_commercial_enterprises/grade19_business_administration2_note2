# 一、反向代理与负载平衡

## 1、代码执行

![图片](./img/thirteen/1.png)
![图片](./img/thirteen/2.png)
![图片](./img/thirteen/3.png)
![图片](./img/thirteen/4.png)
![图片](./img/thirteen/5.png)
![图片](./img/thirteen/6.png)
![图片](./img/thirteen/7.png)

## 2、日志文件

### A、CentOS8的日志 

![图片](./img/thirteen/8.png)
![图片](./img/thirteen/9.png)
![图片](./img/thirteen/10.png)

## 3、理论知识

### A、日志文件说明

+ 常见得日志文件文件名包括登录者得部分信息，因此日志文件得权限通常设置为仅有root能够读取而已；

+ /var/log/boot.log是开机的时候系统内核会去检测与启动硬件，启动各种内核支持的功能，但是这个文件储存本次开机启动的信息；

+ /var/log/dmesg是内核检测过程过程所产生的各项信息，将开机启动是内核的硬件检测过程取消显示；

+ /var/log/maillog或者/var/log/mail/*是记录邮件的往来信息。SMTP是发送邮件所使用的通讯协议，POP3则是接收有见使用的通讯协议；

+ /var/log/messages是几乎系统发生的错误信息都会记录在这个文件中，如果系统发生莫名的错误时，这个文件是一定要查看的日志文件之一；

### B、日志文件所需相关服务与程序

+ 一种是由软件开发商自行定义写入的日志文件与相关格式，另一种是由liunx发行版提供的日志文件管理服务来统一管理的

#### 针对日志文件所需的功能，我们需要的服务与程序有：

+ systemd-journal.service：最主要的信息记录者，有systemd提供；

+ rsyslog.service：主要收集登录系统与网络等服务的信息；

+ logrotate：主要在进行日志文件的轮循功能。