# 第一次笔记
## 1.Linux计划任务的种类：at，cron
```
at:突发性的（仅执行一次就结束的命令）
cron：例行性的（循环执行）
```
## 2.应用安装与删除

```
1.安装：yum install -y(直接确认安装)at
2.删除：yum remove -y(直接确认删除)at
```

## 3.查看服务的状态

```
1.systemctl status atd
```

![lmt](./img/1.png)
![lmt](./img/2.png)

## 4.服务的启动/结束/重启
```
1.启动：systemctl start atd
2.结束：systemctl stop atd
3.重启：systemctl restart atd
```
![lmt](./img/3.png)
![lmt](./img/4.png)
## 5.服务的自启动/关闭自启动
```
1.自启动:systemctl enable atd
2.关闭自启动：systemctl disable atd
```

![lmt](./img/5.png)