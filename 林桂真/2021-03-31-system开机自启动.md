# 今日笔记
## systemd注意事项
```
如果配置文件没有出错，但是查看状态出错，那就可能是端口被占用，修改端口即可。
```
![lmt](./img/28.png)
![lmt](./img/29.png)
## 如何使服务开机自启动
```
在配置文件里面加入:
[Install]
WantedBy=multi-user.target
(设置自启动)
PS：先把文件配置好了，再设置开机自启动的服务（enable）即可。
```

![lmt](./img/30.png)
![lmt](./img/31.png)