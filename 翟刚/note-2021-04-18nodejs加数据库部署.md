## 部署带数据库的nodejs 
### 1. 创建一个数据库
```
su postgres 切换超级管理员
psql 登录数据库
create database miss;    创建数据库
\c miss 进入数据库
\password 给数据库设置密码
\q 退出登录数据库 

```
### 2.把我们的项目放在/var/www/miss.zgxty.top
![如图](./img/21.png)

在miss.zgxty.top 我是直接用git克隆项目在里面

如果想克隆，可以先下载git

命令:yum install -y git

然后：git clone 克隆地址

### 3.修改端口，数据库名字密码，设置公网ip 解析一个域名

在vim db.js 里面数据库名字密码，设置公网ip

![如图](./img/22.png)

阿里云端口开放一个给js用然后设置端口号

![如图](./img/23.png)

### 4.配置文件 /etc/nginx/conf.d 里面

![如图](./img/24.png)

接下来就是检查配置文件 nginx -t

重新加载配置 nginx -s reload

### 5.因为里面有数据库 所有我们要用到 分布式运算程序的资源调度系统 yarn
下载命令 npm i -g yarn

下载完成直接执行 /var/www/miss.zgxty.top/my_koa_sequelize_app/

执行 yarn

### 6.使用pm2让他在服务器上跑起来
命令 pm2 start 路径