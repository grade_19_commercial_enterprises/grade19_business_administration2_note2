## 嘿嘿🤭
### 第一步
```
在/var/www/里面创建一个文件夹一般是域名，(比如heihei.com)然后将js文件
放入创建的文件夹里面
```
### 第二步配置文件
```
在/etc/nginx/conf.d里面创建一个后缀名为.conf的文件
server {
        listen 80;
        server_name heihei.com
    location / {
        proxy_pass http://127.0.0.1:3000

    }

}


```
### 最后一步执行js
```
node /var/www/heihei.com/mian.js
```
测试成功