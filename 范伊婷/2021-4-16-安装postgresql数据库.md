# 安装postgresql数据库（centOS）
1. 首先先安装一个安装包（百度：Linux下安装postgresql）,然后进入官网。如下图。

![图片](./img/postgresql.1.png)

![图片](./img/postgresql.2.png)

![图片](./img/postgresql.3.png)

![图片](./img/postgresql.4.png)

2. 进入Linux，按照上一张图的步骤去做就好了，（注：不要写前面的sudo）,如下两张图。

![图片](./img/postgresql.5.png)

![图片](./img/postgresql.6.png)

3. 安装好了， 我们来查看一下是否有安装成功。

![图片](./img/postgresql.7.png)

4. 成功了，接下来我们对这个数据库进行配置

![图片](./img/postgresql.8.png)

5. 配置pg_hba.conf（vim pg_hba.conf）,在最后添加如图。

![图片](./img/postgresql.9.png)

6. 配置postgresql.conf（vim postgresql.conf），去掉listen和port前面的注释，把''里改成*，5432是端口号，可以自行修改。

![图片](./img/postgresql.10.png)

7. 重新加载一下，再看看状态。

![图片](./img/postgresql.11.png)

8. 可以运行，su postgres,\psql进入数据库,咱们来设置一下密码和创建一个数据库。\l 查看，能看到刚刚自己创的就可以了。（\q 是退出数据库的命令）

![图片](./img/postgresql.12.png)

9. 接下来我们去验证一下，【对了刚刚我们配置的port端口号记得去阿里云添加一下哦】，

![图片](./img/postgresql.13.png)

![图片](./img/postgresql.14.png)

![图片](./img/postgresql.15.png)

10. 连接下的postgreSQL。
![图片](./img/postgresql.16.png)

![图片](./img/postgresql.17.png)

11. 点击连接测试，如下图那样就算成功了，确定再确定，就ok啦！
![图片](./img/postgresql.18.png)

## 好了，postgreSQL数据库的安装就到这。Bye!

