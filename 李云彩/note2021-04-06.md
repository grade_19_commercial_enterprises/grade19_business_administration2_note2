# linux 

## 开机自启动

+ 在cd /etc/systemd/system 目录底下创建一个配置文件king.service 

![好好学习报效祖国](./img/note2021-04-0601.png)

+ 在vim king.service 里进行编辑

+ 编辑内容

```
[Unit]
Descriptipon=king

[Service]
ExecStart=/usr/bin/node  /var/www/king/main.js
WorkingDirectory=/var/www/king
Restart=always
StandardOutput=Syslog
StandardError=Syslog
SyslogIdentifier=nodeapp

[Install]
WantedBy=multi-user.target

```

![好好学习报效祖国](./img/note2021-04-0602.png)

+ systemctl daemon-reload 重新加载某个服务的配置
+ systemctl status king.service 查看该服务是否启动 
+ shutdown -r now 立即重新启动

![好好学习报效祖国](./img/note2021-04-0603.png)

+ systemctl enable king.server 设置开机自启动服务
+ node /var/www/king/main.js 重新运行js文件 

![好好学习报效祖国](./img/note2021-04-0604.png)

+ vim main.js 修改端口
+ systemctl daemon-reload 重新加载某个服务的配置文件
+ systemctl status king.service 重新查看该服务是否开机自启动

![好好学习报效祖国](./img/note2021-04-0605.png)

+ 重新运行js文件

![好好学习报效祖国](./img/note2021-04-0606.png)

+ 去浏览器重新该服务是否跑起来

![好好学习报效祖国](./img/note2021-04-0607.png)