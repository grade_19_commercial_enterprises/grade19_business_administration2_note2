# 上课日常随身笔记
## 一.自定义服务端，配置

![这里的文字是当图片无法显示的时候展示的文本](./img/note2021-04-13-001.png)

![这里的文字是当图片无法显示的时候展示的文本](./img/note2021-04-13-002.png)

## 二.重启、查看rsyslog服务

![这里的文字是当图片无法显示的时候展示的文本](./img/note2021-04-13-003.png)

## 三.自定义客户端，配置

![这里的文字是当图片无法显示的时候展示的文本](./img/note2021-04-13-004.png)

## 四.重启、查看rsyslog服务

![这里的文字是当图片无法显示的时候展示的文本](./img/note2021-04-13-005.png)

## 五.挂起服务端的message

![这里的文字是当图片无法显示的时候展示的文本](./img/note2021-04-13-006.png)

## 六.在客户端logger下

![这里的文字是当图片无法显示的时候展示的文本](./img/note2021-04-13-007.png)

## 七.呈现效果

![这里的文字是当图片无法显示的时候展示的文本](./img/note2021-04-13-008.png)
