# 日志文件服务器的设置
## 客户端
```
cd /etc/rsyslog.d

vim chen.conf

*.* @@服务器

systemctl restart rsyslog.service

systemctl status rsyslog.service

logger "内容"
```
## 服务端
```
vim /etc/rsyslog.conf
找到Tcp删掉#

systemctl restart syslog.service

systemctl status syslog.service

cd /var/log

tail -f mess
```

# 日志文件的轮循

![huyi](./img/2021-04-14_110309.png)
## 后续请看下章