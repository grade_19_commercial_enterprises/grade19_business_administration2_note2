# 2021年03月31日

## 开机自启动
```
    1. cd /etc/systemd/system           进入用户级服务配置
    2. vim xxx-server.service           编辑配置文件
        （在原基础上添加一项）
            [Install]
            WantedBy=multi-user.target
    3. systemctl daemon-reload          重置
    4. systemctl enable xxx-server      设置开机自启动
    5. shutdown -r now                  立即重启
        （若显示未找到命令，即为环境变量的问题）
            到家目录下whereis shutdown
            找到后cd到/usr/sbin下
            再执行立即重启命令./shutdown -r now
    6. 断开后等待一分钟左右后重新连接即可               
```

#### 如图所示
![图片](./img/note2021-03-31_01.png)

![图片](./img/note2021-03-31_02.png)