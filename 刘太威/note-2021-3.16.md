## postgresql 安装,建库，尝试远程连接

## 先去官网安装postgresql数据库
```
1.不自动启动postgresql数据库

2.下载postgresql-13数据库服务

3.下载postgresql13版本的数据库

4.开机自启动postgresql数据库

5.重启postgresql数据库
```
![如图所示](./imgs/010.png)

### 切换postgresql用户--postgres

### 远程连接输入psql

![如图所示](./imgs/011.png)

### 创建数据库用户,修改用户密码
···
create database zhou;
\password

···
![如图所示](./imgs/012.png)

### 退出数据库，切换到root
```
\q
su
```
![如图所示](./imgs/013.png)

### 进入data目录
```
cd /var/lib/pgsql/13/data
```
### 编辑pg_hba.conf配置文件
![如图所示](./imgs/014.png)

### 编辑postgresql.conf配置文件
![如图所示](./imgs/015.png)