## 第一次课堂笔记^-^
### 第15章 计划任务 （crontab）

## 在课本上看到的内容及预习：
### 1. Linux计划任务的种类：
```
例行性：周期性的，使用crontab命令执行，也可以编辑/etc/crontab来执行，让crontab生效的服务是crond。
突发性：只执行一次的命令，使用at，需要atd服务支持，centos默认启动这个服务。
```
### 2.Linux系统上常见的例行性工作 ：在线更新，自动做日志分析等。

### 3. at的运行方式：用at命令产生要运行的任务，然后将此任务以文本文件的方式写入/var/spool/at/目录内，该任务就等待atd服务的执行了。

### 4. 对at的使用限制：/etc/at.allow和/etc/at.deny,若两个文件都不存在则只有root能执行at。

### 5. at -c后面接命令，at now +5 minutes

### 6. atq：查询主机上的at计划任务。

### 7. atrm：删除错误的命令。

### 8.batch：系统不忙时才执行后台命令。当CPU的负载小于0.8时，才执行任务，负载跟任务数有关跟使用率无关，一个任务也可以使用率为100%。

### 9. uptime:可以查看1分钟，5分钟，15分钟的平均任务负载量（load average）

### 10. 限制使用crontab的用户账户，可以使用配置文件：/etc/cron.allow和/etc/cron.deny

### 11.当使用crontab命令建立任务之后，任务就会被记录到/var/spool/cron/中，例如user1使用crontab后，它的任务呗记录在/var/spool/cron/user1中

### 12. cron执行的每一项任务还会被记录到 /var/log/cron这个日志文件中。可以用来查找木马。

### 13. crontab -e 用户编辑任务，其实是/usr/bin/crontab这个执行文件。

### 14. 系统的例行性任务，编辑/etc/crontab文件即可。它是一个纯文本文件。

### 15. cron的最低检测限制是分钟，cron会每分钟读取一次/etc/crontab与/var/spool/cron里面的数据内容。

### 16. crond服务读取配置文件的位置：
    /etc/crontab
    /etc/cron.d/*
    /var/spool/cron/*

### 17. 开发自己的新软件， 想拥有自己的crontab定时命令时，可以将配置文件脚本放置到/etc/cron.d/目录下。

### 18. 除了自己指定分，时，日，月，周，加上命令路径的crond配置文件之外，还可以直接将命令放置到/etc/cron.hourly/目录下，这样该命令就会被crond在每小时的前五分钟的任意一分钟来执行，不需要指定分时日等。

### 19. 个人化操作使用crontab -e，系统维护管理使用vim /etc/crontab，自己开发的软件使用/etc/cron.d/newfile。

### 20. anacron:唤醒停机期间的工作任务，即超过时间未执行的任务。

### 21. anacron默认会以一天，七天，一个月为期去检测系统为执行的crontab命令。

### 22. anacron是一个程序，而不是服务。在centos中会被每小时执行一次，/etc/cron.hourly/里的anacron会在文件名之前加0（0anacron），0是让时间戳更新，防止误判时间。

## 课上讲的内容：

## 一. Linux计划任务的种类：
```
例行性：周期性的，使用crontab命令执行，也可以编辑/etc/crontab来执行，让crontab生效的服务是crond。
突发性：只执行一次的命令，使用at，需要atd服务支持，centos默认启动这个服务。
```
1. systemctl status atd ：首先要先查看一下状态，看看是否有启动
##### 注：有出现 running就代表正在运行，说明就不需要再去启动了;出现dead就代表没有在运行，此刻我们就需要写下启动命令.
2. systemctl start atd ：开启at
3. systemctl enable atd ：如果你觉得每次都要手动开启at很麻烦的话，这个就是让这个服务开机后就自动启动
4. systemctl disable atd ：跟3相反，取消开机后自动启动

## 一.仅执行一次的计划任务：at
```
at命令是一次性定时计划任务，at的守护进程atd会以后台模式运行，检查作业队列来运行作业。atd守护进程会检查系统上的一个特殊目录来获取at命令的提交的作业，默认情况下，atd守护进程每60秒检查一次目录，有作业时，会检查作业运行时间，如果时间与当前时间匹配，则运行此作业。
    注意：at命令是一次性定时计划任务，执行完一个任务后不再执行此任务了。
```
### 关于at的安装和移除
1. yum install at ： 安装at
```
命令“yum remove at”，加上参数-y，自动移除
命令“yum install at”，不加参数-y，需手动选择
如果按回车键，则操作失败，也是没安装成功
“y”代表确认安装；“N”代表取消安装；系统默认值有可能是y"也有可能是“N”
```
2. yum remove at ： 卸载at
```
命令“yum remove at”，加上参数-y，自动移除
```

## 二. 循环执行的计划任务 cron
```
例行性，这个命令所设置的任务将会循环的一直执行下去，最小的循环时间单位是分钟。执行cron时，必须要有crond服务的支持。
```
1. systemctl status crond ：查看状态，看看是否有启动
2. systemctl start crond ：开启cron
3. systemctl enable crond ：开机后自动开启
4. systemctl disable crond :取消开机后自动开启
5. yum install cron ： 安装cron
6. yum remove cron ： 移除cron