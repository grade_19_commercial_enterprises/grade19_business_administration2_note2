# 任务管理
## 1.at命令
+ 查看at服务状态：systemctl status atd
+ 启动at服务：systemctl start atd
+ 重新启动：systemctl restart atd
+ 开机自启动at服务：systemctl enable atd
+ 开机不自启动：systemctl disable atd
+ 停止at服务：systemctl stop atd
+ 删除任务：atrm +任务编号
### 软件的卸载和安装（centos8）
+ 软件的安装 ：yum -y install 软件名
+ 软件的卸载 ：yum -y remove 软件名
```
    at [option] [time]
    Ctrl + D 结束at命令输出
    在使用at命令的时候一定要保证atd进程的启动

    [option]:
        -m              当指定的任务被完成后，将给用户发送邮件，即使没有标准输出
        -l              atq的别名
        -d              atrm的别名
        -v              显示任务将被执行的时间
        -c              打印任务的内容到标准输出
        -V              显示版本信息
        -q<队列>        使用指定的队列
        -f<文件>        从指定文件读入任务而不是从标准输出读入
        -t<时间参数>     以时间参数的形式提交要运行的任务
```

### 时间定义
```
    at允许使用一套相当复杂的指定时间的方法：
    1.  能够接受在当天的hh：mm（小时：分钟）式的时间指定。加入该事件已过去，那么就放在第二天执行。例如：04：00。
    2.  能够使用midnight（深夜），noon（中午），teatime（饮茶时间，一般时下午4点）等比较模糊的词语来指定时间。
    3.  能够采用12小时计时制，即在时间后面加上AM（上午）或PM（下午）来说明是上午还是下午（12pm）。
    4.  能够制定命令执行的具体日期，制定格式为month day（月 日）或mm/dd/yy（月/日/年）或dd.mm.yy（日.月.年）,指定的日期必须跟在指定时间的后面。 例如：04：00 2009-03-1.
    5.  能够使用相对计时法。指定格式为：now + count time-units，now就是当前时间，time-units时时间单位，这里能够使minutes（分钟）、hours（小时）、days（天）、weeks（星期）。count时时间的数量，几天，几小时。 例如： now + 5 munutes 04pm +3 days。
    6.  能够直接使用today（今天）、tomorrow（明天）来指定完成命令的时间。
```

## 相关命令
1. at：在特定的时间执行一次性的任务；
2. atq：列出用户的计划任务，如果使超级用户将列出所有用户的任务，结果的输出格式为：作业号、日期、小时、队列和用户名。
3. atrm：根据job number删除at任务；
4. batch：在系统负荷允许的情况下执行at任务，就是在系统空闲的情况下才执行at任务。

## 相关配置文件
1. 时间规范的确切定义可以在/usr/share/doc/at-3.1/timespec中查看；
2. 默认情况下计划任务都是存放在/var/spool/at文件夹里；
3. root用户可以在任何情况下使用at命令，而其他用户使用at命令的权限定义在/etc/at/allow(被允许使用计划任务的用户)和/etc/at.deny（被拒绝使用计划任务的用户）文件中，默认没有文件需要自己创建允许用户和拒绝用户文件；
4. /etc/at/allow文件存在，只有在该文件中的用户名对应的用户才能使用at；
5. 如果/etc/at/allow文件不存在，/etc/at.deny存在，所有不在/etc/deny文件中的用户可以使用at；
6. at.allow 比at.deny优先级高，执行用户是否可以执行at命令，先看at.allow文件中有没有，然后才看at.deny文件；
如果/etc/at.allow和/etc/at/deny文件都不存在，则只有root用户能使用at；
8. 一个空内容的/etc/at/deny表示任何用户都能使用at命令，这是默认的配置；
9. 一般情况下这两个文件存在一个即可。如果只有少数几个用户需要使用计划任务，那么就保留at.allow文件，如果大部分用户都是用计划任务，那么保留at.deny即可。
