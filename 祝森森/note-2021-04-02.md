#  静态网页部署步骤
1:
```
yum install -y nginx--安装nginx
systemctl enable nginx--设置开机自启动nginx
sysytemctl start nginx--设置立即启动nginx
```
2:
```
mkdir /var/www
cd /var/www
mkdir zz.com
cd zz.com
📤📥拉index.html文件到zz.com
```
3:
```
vim /etc/nginx/conf.d/zz.com.conf--配置文件
   server{
      listen 80;  --监听的端口
      server_name zz.com;  --监听的域名
      location / {
       root /var/www/zz.com;  --网站所在路径
   }
       index index.html;  --默认的首页文件
   }
```
4:
```
   nginx -t  --测试配置文件是否正确(返回successful正确)
   nginx -s reloa 或者systemctl reload nginx  --(重新加载)
```